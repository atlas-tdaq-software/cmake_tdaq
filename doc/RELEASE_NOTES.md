# CMake for TDAQ

## tdaq-common-12-00-00

The `-fwrapv` option has been added to the default
compiler options. This makes sure that signed integer
overflow has defined behaviour rather than UB, which
led to some surprising changes in optimized gcc 14 builds.

### Add new `EXTRA_EXTERNALS` keyword to `tdaq_project()`

Packages referenced in the `EXTRA_EXTERNALS` argument are
added as an RPM dependency to the project. This means they
are automatically installed at Point 1 and other private
installations. This is used for package that are *not* used
by TDAQ but required by some downstream detector project.

### Add each externals path to `CMAKE_PREFIX_PATH`

Each LCG package referenced in the `EXTERNALS` argument to
`tdaq_project()` is automatically added to the cmake prefix
path. This ensures that it can be found even if the LCG package
name differs from the actual upstream package name (Example:
`nlohmann_json` vs. `jsonmcpp`).

### Set CMake Policy [CMP0144](https://cmake.org/cmake/help/v3.29/policy/CMP0144.html) to `NEW`

Given a package `foo` CMake will automatically add the content of
the `${FOO_ROOT}` variable to the search path. That means that
all LCG packages that export CMake config file will be found (as long as
the LCG package names is the same as upstream), as variables of
that form have always been defined by `cmake_tdaq`.

It might also pick-up unwanted stuff. In this case one can either
add a `cmake_policy(SET CMP0144 OLD)` on the appropriate level, or
just `unset(FOO_ROOT CACHE)` where needed.

An example is `git` which is part of LCG. However, that version
does not work properly without some environment set to the right
paths (as git contains hard-coded paths determined at build time).
Functions like `ExternalProject_Add()` may fail in that case.

### Use Boost native cmake config files instead of FindBoost.cmake

This should be mostly compatible as long as the Boost targets are used.

### Use ROOT native cmake config files instead of FindROOT.cmake

Again this should be compatible as long as ROOT targets are used.

### Compressed debug sections

Debug sections are compressed with zlib. The \*.debug files are
put into a `.debug` subdirectory of `bin` or `lib` rsp. They
are found transparently by `gdb` and other tools which need them.

### Initial support for C++20 modules

This only works (for some definition of "works") with gcc >= 14
and Ninja instead of Make. Note that gcc 13 is most likely the
default compiler for 2025, so this should be restricted to
private tests. Ninja is included in the PATH if it
found at `cm_setup` time.

```shell
cm_setup nightly gcc14
cmake -B build -G Ninja
cmake --build build -j
```

### Enable monolithic RPM generation, make src install optional

Downstream projects should be able to call `cpack` after a
build and get a usable tar file or monolithic RPM.

```cmake
cmake_minimum_required(VERSION 3.29.0)

# We make both detector and TDAQ version variables so the
# can be overridden from the command line for test builds
# cmake -D TDAQ_VERSION=99.0.0 ...
set(DET_VERSION 1.0.0 CACHE STRING "DET Release version")
set(TDAQ_VERSION 11.2.0 CACHE STRING "TDAQ Release version")

project(DET VERSION ${DET_VERSION} LANGUAGES C CXX)

# Optional, leave out if you want source code installed into project
# this has to come *before* find_package(TDAQ)
option(TDAQ_INSTALL_SOURCE "Disable full source code installation")

find_package(TDAQ)
include(CTest)

# This enables a monolithic RPM or tar file
set(CPACK_MONOLITHIC_INSTALL TRUE)

# optional, similar for other variables
set(CPACK_RPM_PACKAGE_DESCRIPTION "My DET project")

# Note: For RPM installation, tdaq, zeromq and protobuf are all dependencies
# and are automatically installed.
tdaq_project(DET ${DET_VERSION}
             USES tdaq ${TDAQ_VERSION}
             EXTERNALS zeromq
             EXTRA_EXTERNALS protobuf)
```
