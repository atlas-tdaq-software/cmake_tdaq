#!/bin/bash
#
# Run a native genconfig command from a given release
#
# usage:
#   genconfig.sh <native_tdaq_installation> <native_tdaq_configuration> <tdaq_db_path> <genconfig arguments...>
#
export CMTCONFIG=$2
source $1/setup.sh
export TDAQ_DB_PATH=$3
shift 3
genconfig $@
