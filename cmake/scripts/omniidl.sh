#!/bin/bash
#
# Run a native omniidl command from a given release
#
# usage:
#   genconfig.sh <native_tdaq_installation> <native_tdaq_configuration> <omniidl_arguments...>
#
export CMTCONFIG=$2
source $1/setup.sh
shift 2
tdaq_python $TDAQ_INST_PATH/share/bin/omniidl $@
