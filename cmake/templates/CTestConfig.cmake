set(CTEST_PROJECT_NAME "@PROJECT@")
set(CTEST_NIGHTLY_START_TIME "00:00:00 CEST")

set(CTEST_SUBMIT_URL "https://atlas-tdaq-cdash.web.cern.ch/submit.php?project=@PROJECT@")

set(CTEST_DROP_METHOD "https")
set(CTEST_DROP_SITE "atlas-tdaq-cdash.web.cern.ch")
set(CTEST_DROP_LOCATION "/submit.php?project=@PROJECT@")
set(CTEST_DROP_SITE_CDASH TRUE)

set(CTEST_USE_LAUNCHERS 1)
