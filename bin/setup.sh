# Usage: source setup.sh [config]
# (NOTE: you probably want 'source cm_setup.sh' to setup a work environment for a user)
#
# where 'config' is using the CMTCONFIG standard syntax: ARCH-OS-COMPILER-BUILD
# e.g. x86_64-centos7-gcc11-opt.
#
# If config is not given the CMTCONFIG environment variable must be set.
#

# You can override in the environment before sourcing this:
#
# CMAKE_PROJECT_PATH : where the TDAQ projects live we compile against
# TOOL_BASE          : where the TDAQ specific tools live (CMake, git)
# CMAKE_VERSION      : the default CMake version to use
# CMAKE_BASE         : where to find CMake versions
# CMAKE_PATH         : the explicit path to a 'cmake' you want to use
#

# The first part of this script is always executed; it defines:
#
# CMAKE_PREFIX_PATH (guided by CMAKE_PROJECT_PATH)
# TDAQ_HOST_ARCH    : the arch-os tuple used on the host (in case of cross-compiling different from target)
#

# For TDAQ projects
CMAKE_PROJECT_PATH=${CMAKE_PROJECT_PATH:=/cvmfs/atlas.cern.ch/repo/sw/tdaq}
[ -d /sw/atlas/tdaq ] && CMAKE_PROJECT_PATH=/sw/atlas

# Determine <arch>-<os>, used to find various paths for basic binutils and compilers
# without the need for the <compiler>-<opt|dbg> part of the tag.
export TDAQ_HOST_ARCH=${TDAQ_HOST_ARCH:=$($(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}}))/getcmtconfig -hw)}

# Setup CMAKE_PREFIX_PATH to find other projects
export CMAKE_PREFIX_PATH=${CMAKE_PROJECT_PATH}:$(dirname $(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}})))/cmake

# The location of CMake and git, if not taken from LCG
TOOL_BASE_DEFAULT=/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/${TDAQ_HOST_ARCH}
[ -d /sw/tdaq/tools ] && TOOL_BASE_DEFAULT=/sw/tdaq/tools/${TDAQ_HOST_ARCH}

TOOL_BASE=${TOOL_BASE:=${TOOL_BASE_DEFAULT}}

# The location and version of CMake to use
CMAKE_VERSION=${CMAKE_VERSION:=3.31.0}
CMAKE_BASE=${CMAKE_BASE:=${TOOL_BASE}/CMake}
CMAKE_PATH=${CMAKE_PATH:=${CMAKE_BASE}/${CMAKE_VERSION}/bin}

# Setup path for CMAKE
export PATH=$(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}})):${CMAKE_PATH}:${PATH}

unset TOOL_BASE TOOL_BASE_DEFAULT CMAKE_BASE CMAKE_VERSION CMAKE_PATH

# If we have sourced the run-time setup via cm_setup.sh we already have
# the compiler, we are done
[ -n "${TDAQ_LCG_RELEASE}" ] && return 0

# The second part tries to figure out the correct compiler in case
# we are doing a bootstrap build of a project.

# Default LCG release to determine compilers from
lcg=LCG_107

# This can still be overriden on the command line with the --lcg=... option
case "$1" in
    --lcg=*)
        lcg=${1#--lcg=}
        shift
        ;;
    *)
        ;;
esac

_conf=${1}
if [ -z "${_conf}" ]; then
   _conf=${CMTCONFIG}
fi

if [ -z "${_conf}" ]; then
   echo "You must specifiy a configration on the commandline or in \$CMTCONFIG"
   return 1
fi

DEFAULT_LCG_BASE="/cvmfs/sft.cern.ch/lcg/releases"

# Point 1
[ -d /sw/atlas/sw/lcg/releases ] && DEFAULT_LCG_BASE=/sw/atlas/sw/lcg/releases

# dev3/4 nightlies
case "${lcg}" in
    dev*)
       DEFAULT_LCG_BASE="/cvmfs/sft-nightlies.cern.ch/lcg/nightlies"
       ;;
esac

# you can still override this defining LCG_RELEASE_BASE beforehand
export LCG_RELEASE_BASE=${LCG_RELEASE_BASE:=${DEFAULT_LCG_BASE}}
LCG_BASE=$(dirname ${LCG_RELEASE_BASE})

# contrib LCG area
CONTRIB_BASE=${CONTRIB_BASE:=${LCG_BASE}/contrib}

# setup PATH for ninja if it exists
NINJA_VERSION=${NINJA_VERSION:=1.11.1}
NINJA_BASE=${NINJA_BASE:=${CONTRIB_BASE}/ninja}
NINJA_PATH=${NINJA_PATH:=${NINJA_BASE}/${NINJA_VERSION}/$(uname)-$(arch)/bin}
[ -d "${NINJA_PATH}" ] && export PATH=${NINJA_PATH}:${PATH}

############## end of configuration variables ################

# Choose compiler, this has to reflect what you choose in the tag to
# cmake_config later
case "${_conf}" in
    *-gcc*)
        if [ -n "${lcg}" ] && [ -f "${LCG_RELEASE_BASE}/${lcg}/LCG_externals_${_conf}.txt" ]; then
            gcc_requested=$(grep '^COMPILER:' ${LCG_RELEASE_BASE}/${lcg}/LCG_externals_${_conf}.txt | cut -d' ' -f2 | cut -d';' -f2)
        else
            gcc_requested=$(echo "${_conf}" | cut -d- -f3 | tr -d 'gcc')
        fi
        if [ -f ${CONTRIB_BASE}/gcc/${gcc_requested}binutils/${TDAQ_HOST_ARCH}/setup.sh ]; then
            source ${CONTRIB_BASE}/gcc/${gcc_requested}binutils/${TDAQ_HOST_ARCH}/setup.sh 
        elif [ -f ${LCG_RELEASE_BASE}/gcc/${gcc_requested}binutils/${TDAQ_HOST_ARCH}/setup.sh ]; then
            source ${LCG_RELEASE_BASE}/gcc/${gcc_requested}binutils/${TDAQ_HOST_ARCH}/setup.sh
        elif [ -f ${CONTRIB_BASE}/gcc/${gcc_requested}/${TDAQ_HOST_ARCH}/setup.sh ]; then
            source ${CONTRIB_BASE}/gcc/${gcc_requested}/${TDAQ_HOST_ARCH}/setup.sh
        elif [ -f ${LCG_RELEASE_BASE}/gcc/${gcc_requested}/${TDAQ_HOST_ARCH}/setup.sh ]; then
            source ${LCG_RELEASE_BASE}/gcc/${gcc_requested}/${TDAQ_HOST_ARCH}/setup.sh
        else
            gcc_version=$(g++ --version | head -1 | awk '{ print $NF; }' | cut -d. -f1)
            if [ "${gcc_version}" -ne $(echo "${gcc_requested}" | cut -d. -f1) ]; then
               echo "warning: gcc version ${gcc_requested} needed, but version ${gcc_version} found" 1>&2
            fi
        fi
        unset gcc_requested gcc_version
        ;;
    *-clang*)
        if [ -n "${lcg}" ] && [ -f ${LCG_RELEASE_BASE}/${lcg}/LCG_externals_${_conf}.txt ]; then
            clang_requested=$(grep '^COMPILER:' ${LCG_RELEASE_BASE}/${lcg}/LCG_externals_${_conf}.txt | cut -d' ' -f2 | cut -d';' -f2)
        else
            clang_requested=$(echo "${_conf}" | cut -d- -f3 | tr -d 'clang')
        fi
        if [ -f ${CONTRIB_BASE}/clang/${clang_requested}/${TDAQ_HOST_ARCH}/setup.sh ]; then
            source ${CONTRIB_BASE}/clang/${clang_requested}/${TDAQ_HOST_ARCH}/setup.sh
        elif [ -f ${LCG_RELEASE_BASE}/clang/${clang_requested}/${TDAQ_HOST_ARCH}/setup.sh ]; then
            source ${LCG_RELEASE_BASE}/clang/${clang_requested}/${TDAQ_HOST_ARCH}/setup.sh
        fi
        unset clang_requested
        ;;
esac

export gcc_config_version

unset  LCG_BASE CONTRIB_BASE LCG_BASE_DEFAULT _conf lcg
